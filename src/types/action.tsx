import { product } from "./type";

// action strings
export const tes_phone = "TEST";


export interface Test {
    type: typeof tes_phone;
    products: product[];
}
export type PhoneActionTypes =Test;
   

export type AppActions = PhoneActionTypes;