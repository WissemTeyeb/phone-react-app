import Product from "../components/Product"

export interface product{
    id: number,
    title: string,
    img: string,
    price: number,
    company: string,
    info: string,
    inCart: boolean,
    count: number,
    total: number,
    
 }

 export interface DataReturn {

     productus: product[] 
     details:product
 }

