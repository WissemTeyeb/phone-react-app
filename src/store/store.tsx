import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk, { ThunkMiddleware } from "redux-thunk";
import { storeProducts, detailProduct } from '../data'
import { DataReturn } from "../types/type";
import { AppActions } from "../types/action";

import { PhoneActionTypes } from "../types/action";

const returnData: DataReturn={
    productus:[],
    details: detailProduct,

}

const phoneReducer = (state = returnData, action: PhoneActionTypes): DataReturn  => {
  
    if (action.type === "TEST") {
        return {
            productus:storeProducts,
            details: detailProduct
        }   ;
    }
    else{
     return state

    }
 
}


const rootReducer = combineReducers({
    products: phoneReducer,
   
});

const Store = createStore(
    rootReducer,
    applyMiddleware(thunk as ThunkMiddleware<AppState, AppActions>)
    );
export type AppState = ReturnType<typeof rootReducer>;

export default Store;
