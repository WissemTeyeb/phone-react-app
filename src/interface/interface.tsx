import Product from "../components/Product"
import { storeProducts } from '../data'


export interface struct 

{

    name: string;
    title: string;
}

export type products={
    id: number,
    title: string,
    img: string,
    price: number,
    company: string,
    info: string,
    inCart: boolean,
    count: number,
    total: number,
    
 }[]

export interface state {
  loggedIn: boolean;
  session: string;
  userName: string;
}

// Describing the different ACTION NAMES available
export const action = "TEST";

interface action {
    type: typeof action;
    payload: products ;
}

export type phoneAction = action;

