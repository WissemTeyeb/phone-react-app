
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from '../logo.svg';
import "font-awesome/css/font-awesome.min.css";
import '../App.css';
import styled from "styled-components";
export class Navbar extends Component {
    
    render() {

        return (
            <NavWrapepr className="navbar navbar-expand-sm  navbar-dark px-sm-5">             
                        
                <Link to="/">

                    <img src={logo} alt="store" className="navbar-brand" ></img>
                </Link>  

                
                <ul className="navbar-nav align-items-center">
               
               
                   
<li className="nav-item ml-5">
    <Link to="/" className="nav-link">
        products
        </Link>
</li>
</ul>
<ul>



    
</ul>
              {   <Link to="/card" className="nav-auto">
                    <ButtonContainer >
                        <span className="mr-1">
                            <i className="fas fa-camera" />
                        </span>
                        My cart
                    </ButtonContainer>
                </Link> }
            </NavWrapepr>
            
        );

    }


}
 const ButtonContainer = styled.button`
  text-transform:capitalize;
   font-size: 1.4rem;
   background :transparent;
   border:0.05rem solid var(--lightBlue);
   color:var(--lightBlue);
   border-radius: 0.5rem;
   padding:0.2rem 0.5rem;
   cursor: pointer;
   margin:0.1rem 0.5rem 0.2rem 0;
   transition: all 0.5s ease-in-out;
   &:hover{
       background:var(--lightBlue);
              color:var(--mainBlue)

   }
    &:focus{
       outline:none;
   }
`;
const NavWrapepr=styled.nav`

       background:var(--mainBlue);
       color:var(--mainWhite)!important;
       font-size:1.3rem;
         text-transform:capitalize;



`
export default Navbar;