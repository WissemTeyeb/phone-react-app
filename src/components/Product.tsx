
import React, { Component, MouseEvent  } from 'react';
import { product } from "../types/type";
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Button } from 'react-native';

interface props {
    product: product
     details:product

}

type NewType = HTMLImageElement;

export class Product extends Component<props> {
    handleClick(event: MouseEvent<NewType>) {
    console.log('you click me in the image container')
    }
    render() {
        const {  product, details } = this.props;
        return (
            < ProductWrapper className="col-9 mx-auto col-md-6 col-lg-3 my-3">
           
           <div className="card">
                <div className="img-container p-5" onClick={ this.handleClick }>

                        <Link to="/details">
<img src={product.img} alt="product" className="card-img-top"></img>
     
</Link>
<button className="cart-btn" disabled={product.inCart ? true : false}
 onClick={()=>{console.log("added in the cart");
                        }} >
 {product.inCart ? (
 <p className="text-capitalize mb-0"  >
    
     in inCart
     </p>
 ) : (
 <i className="fas fa-cart-plus"/>
 )}
</button>
  </div>
  {/* card footer */}
  <div className="card-footer d-flex justify-content-between">
      <p className="align-self-center">
          {product.title}
      </p>
      <h5 className="text-blue font-italic mb-0">
<span className="mr-1">$</span>
{product.price}

      </h5>
  </div>
           </div>
           
            </ProductWrapper>


        )

    }


}
const ProductWrapper=styled.div`
.card{
    border-color:transparent;
    transition:all 1s linear;

}
.card-footer{
    background:transparent;
    vorder-top:transparent;
    transition:all 1s linear
}
&:hover{
    .card{
        border:0.4rem solid rgba(0,0,0,0.2);
        box-shadow: 2px 2px 5px 0px rgba(0,0,0,0.2)
    }
    .card-footer{
        background:rgba(247,247,247);

    }

}
.img-container{
    position:relative;
    overflow:hidden;

}
.card-img-top{
    transition:all 1s linear;
}
.img-container:hover .card-img-top{
    transform:scale(1.2);
}
.cart-btn{
    position:absolute;
    bottom:0;
    right:0;
    padding:0.2rem 0.4rem;
    background:var(--lightBlue);
    border:none;
    color:var(--mainWHite);
    font-size:1.4rem;
    border-radius:0.5rem 0 0 0;
    transform:translate(100%,100%);
        transition:all 1s linear;


}
.img-container:hover .cart-btn{
    transform:translate(0,0);
}
.cart-btn:hover{
    color:var(--mainBlue);
    cursor:pointer;
}
`
export default Product