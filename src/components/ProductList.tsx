
import React from 'react';
import Title from './title'
import { connect } from "react-redux"
import { AppState } from "../store/store";
import { Dispatch } from "redux";
import { DataReturn } from "../types/type";

import { AppActions } from '../types/action';
import {  testPhone } from '../actions/products';
import Product from './Product';

interface LinkDispatchProps {
    startTestPhone: () => void;

}


type Props = DataReturn & LinkDispatchProps;
export class List extends React.Component<Props>   {
    componentDidMount() {
        
        this.props.startTestPhone()
    }
    render() {
        console.log(this.props.productus)
        console.log(this.props.details)

            return (
              <React.Fragment >
                  <div className="py-5">
                      <div className="container">
                        <Title name="our" title="products" />
                        <div className="row"/>
                            { this.props.productus.map(data=>{
                                return <Product key={data.id} product={data} details={this.props.details} />;
                            
                            })
                                 
                                 
      }

                        </div>

                      </div>
             </React.Fragment >
        )

    }


}
const mapStateToProps = (
    state: AppState,
): DataReturn => ({
        productus: state.products.productus,
        details: state.products.details
       
});
const mapDispatchtoProps = (dispatch: Dispatch<AppActions>): LinkDispatchProps=> {
    return {
        startTestPhone: () => dispatch(testPhone()),
  
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(List);

