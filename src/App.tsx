import React, { Component } from 'react';
import Default from  './components/Default' ;
import Navbar from './components/Navbar';
import Details from './components/Details' ;
import Product from './components/Product' ;
import List from './components/ProductList' ;
import { Switch, Route } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from './components/Card';


class App extends Component {
  render(){

  return(
    <React.Fragment>
      <Navbar/>
      <Switch>
        <Route  exact path="/" component={List} />

        <Route path="/product" component={Product}/>
        <Route path="/list" component={List}/>
        <Route path="/details" component={Details} />

        <Route path="/card" component={Card}/>
        <Route  component={Default} />

      </Switch>
    </React.Fragment>
  );

  
    }
}
export default App;
